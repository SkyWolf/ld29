﻿using UnityEngine;
using System.Collections;

public class DetachAndExplode : MonoBehaviour {
    private Transform myTransform;
    private Rigidbody2D myRigidbody;
    private TileType myTileType;
    public SpriteRenderer sRenderer;

    void Awake() {
        myTransform = transform;
        myRigidbody = rigidbody2D;
        myTileType = GetComponent<TileType>();
    }

    public void DoIt() {
        myTransform.parent = null;
        myRigidbody.isKinematic = false;
        myRigidbody.AddForce((Vector2.up + (Vector2.right * ((Random.value * 0.8f) - 0.4f))) * 150);

        myTileType.points = 0;
        myTileType.SetType();

        sRenderer.sortingOrder = -1;

        Destroy(gameObject, 4);
    }
}