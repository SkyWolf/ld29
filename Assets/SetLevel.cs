﻿using UnityEngine;
using System.Collections;

public class SetLevel : MonoBehaviour {
    public Transform realBar;

    public void Setup(float level) {
        realBar.localScale = new Vector3(level, 1, 1);
    }
}