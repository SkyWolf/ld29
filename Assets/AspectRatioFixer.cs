﻿using UnityEngine;
using System.Collections;

public class AspectRatioFixer : MonoBehaviour {
    public Vector3[] aspectRatios;
    private Transform myTransform;

    void Awake() {
        myTransform = transform;
        FixAspectRatio();
        StartCoroutine(ARFixer());
    }

    private IEnumerator ARFixer() {
        while(true) {
            yield return new WaitForSeconds(0.1f);
            FixAspectRatio();
        }
    }

    private void FixAspectRatio() {
        DetermineAspectRatio();
        myTransform.localPosition = aspectRatios[toSet];
    }

    private int toSet;
    private void DetermineAspectRatio() {
        float aspect = Camera.main.aspect;
        if(aspect <= 1.26f)
            toSet = 0;
        else if(aspect <= 1.34f)
            toSet = 1;
        else if(aspect <= 1.51f)
            toSet = 2;
        else if(aspect <= 1.61f)
            toSet = 3;
        else
            toSet = 4;
    }
}
