﻿using UnityEngine;
using System.Collections;

public class TileColor : MonoBehaviour {
    public Material[] materials;
    public BlockColors actualColor { get; private set; }

    public SpriteRenderer sRenderer;

    void Awake() {
        actualColor = BlockColors.Air;
    }

    public void SetColor(BlockColors newType) {
        if(newType == actualColor || sRenderer == null)
            return;

        actualColor = newType;
        sRenderer.sharedMaterial = materials[(int)newType];
    }
}
