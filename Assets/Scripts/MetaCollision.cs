﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(PolygonCollider2D))]
public class MetaCollision : MonoBehaviour {
    private PolygonCollider2D polyCollider;
    private Transform myTransform;
    private int blockLayerMask;

    public Transform spriteOffset;

    void Awake() {
        polyCollider = GetComponent<PolygonCollider2D>();
        myTransform = transform;

        upBlocks = new List<Transform>();
        downBlocks = new List<Transform>();
        toTrigger = new List<Collider2D>();
        actualState = FallState.Still;

        blockLayerMask = 1 << LayerMask.NameToLayer("Block");

        CreateMetaCollider();
    }

    private bool ListContainsVector2(List<Vector2> list, Vector2 toCheck) {
        bool contains = false;
        for(int i = 0; i < list.Count; i++) {
            Vector2 element = list[i];
            if(Mathf.Approximately(element.x, toCheck.x) && Mathf.Approximately(element.y, toCheck.y))
                contains = true;
        }

        return contains;
    }

    public void CreateMetaCollider() {
        int childs = spriteOffset.childCount;
        if(childs > 0) {
            List<float> xPositions = new List<float>();
            List<float> yPositions = new List<float>();
            float leftMostX = spriteOffset.GetChild(0).localPosition.x;
            float upMostY = spriteOffset.GetChild(0).localPosition.y;

            for(int i = 0; i < childs; i++) {
                Transform cube = spriteOffset.GetChild(i);

                float x = cube.localPosition.x;
                float y = cube.localPosition.y;

                if(!xPositions.Contains(x)) {
                    xPositions.Add(x);
                    if(x < leftMostX)
                        leftMostX = x;
                }
                if(!yPositions.Contains(y)) {
                    yPositions.Add(y);
                    if(y > upMostY)
                        upMostY = y;
                }

            }

            int xCount = xPositions.Count;
            int yCount = yPositions.Count;
            Transform[,] blocks = new Transform[xCount, yCount];
            for(int i = 0; i < xCount; i++) {
                for(int j = 0; j < yCount; j++) {
                    blocks[i, j] = null;
                }
            }

            for(int i = 0; i < childs; i++) {
                Transform cube = spriteOffset.GetChild(i);

                int realX = Mathf.RoundToInt((cube.localPosition.x - leftMostX));
                int realY = Mathf.RoundToInt((upMostY - cube.localPosition.y));

                blocks[realX, realY] = cube;
            }

            List<Vector2> corners = new List<Vector2>();

            int verticalQuota = 0;
            int horizontalQuota = 0;
            int initialQuota = -1;
            Vector2 newCorner;

            for(int i = 0; i < xCount; i++) {
                int j = 0;
                while(blocks[i, j] == null)
                    j++;

                if(i == 0)
                    initialQuota = j;
                verticalQuota = j;

                newCorner = new Vector2(blocks[i, j].localPosition.x - 0.5f, blocks[i, j].localPosition.y + 0.5f);
                if(!ListContainsVector2(corners, newCorner))
                    corners.Add(newCorner);

                newCorner = new Vector2(blocks[i, j].localPosition.x + 0.5f, blocks[i, j].localPosition.y + 0.5f);
                if(!ListContainsVector2(corners, newCorner))
                    corners.Add(newCorner);
            }

            for(int j = verticalQuota; j < yCount; j++) {
                int i = xCount - 1;
                while(blocks[i, j] == null)
                    i--;

                horizontalQuota = i;

                newCorner = new Vector2(blocks[i, j].localPosition.x + 0.5f, blocks[i, j].localPosition.y + 0.5f);
                if(!ListContainsVector2(corners, newCorner))
                    corners.Add(newCorner);

                newCorner = new Vector2(blocks[i, j].localPosition.x + 0.5f, blocks[i, j].localPosition.y - 0.5f);
                if(!ListContainsVector2(corners, newCorner))
                    corners.Add(newCorner);
            }

            for(int i = horizontalQuota; i >= 0; i--) {
                int j = yCount - 1;
                while(blocks[i, j] == null)
                    j--;

                verticalQuota = j;

                newCorner = new Vector2(blocks[i, j].localPosition.x + 0.5f, blocks[i, j].localPosition.y - 0.5f);
                if(!ListContainsVector2(corners, newCorner))
                    corners.Add(newCorner);

                newCorner = new Vector2(blocks[i, j].localPosition.x - 0.5f, blocks[i, j].localPosition.y - 0.5f);
                if(!ListContainsVector2(corners, newCorner))
                    corners.Add(newCorner);
            }

            for(int j = verticalQuota; j >= initialQuota; j--) {
                int i = 0;
                while(blocks[i, j] == null)
                    i++;

                horizontalQuota = i;

                newCorner = new Vector2(blocks[i, j].localPosition.x - 0.5f, blocks[i, j].localPosition.y - 0.5f);
                if(!ListContainsVector2(corners, newCorner))
                    corners.Add(newCorner);

                newCorner = new Vector2(blocks[i, j].localPosition.x - 0.5f, blocks[i, j].localPosition.y + 0.5f);
                if(!ListContainsVector2(corners, newCorner))
                    corners.Add(newCorner);
            }

            upBlocks.Clear();
            downBlocks.Clear();
            for(int i = 0; i < xCount; i++) {
                int jMin = 0;
                int jMax = yCount - 1;
                while(blocks[i, jMin] == null)
                    jMin++;
                while(blocks[i, jMax] == null)
                    jMax--;

                upBlocks.Add(blocks[i, jMin]);
                downBlocks.Add(blocks[i, jMax]);
            }

            polyCollider.SetPath(0, corners.ToArray());
        }
    }

    private List<Transform> upBlocks;
    private List<Transform> downBlocks;

    private List<Collider2D> toTrigger;
    public void FindAllUpNeighbors() {
        toTrigger.Clear();
        for(int i = 0; i < upBlocks.Count; i++) {
            RaycastHit2D hit = Physics2D.Raycast(upBlocks[i].position + (Vector3.up * 0.51f), Vector2.up, 0.49f, blockLayerMask);
            if(hit.collider != null)
                toTrigger.Add(hit.collider);
        }
    }

    public enum FallState {
        Still,
        Wait,
        Fall
    }
    public FallState actualState { get; private set; }

    void OnDestroy() {
        FindAllUpNeighbors();
        TriggerFallOnUpwards();
    }

    public void DetachEverything() {
        if(spriteOffset != null) {
            int childCount = spriteOffset.childCount;
            for(int i = 0; i < childCount; i++)
                spriteOffset.GetChild(0).GetComponent<DetachAndExplode>().DoIt();
        }
    }

    public void TriggerFallOnUpwards() {
        for(int i = 0; i < toTrigger.Count; i++) {
            if(toTrigger[i] != null)
                toTrigger[i].GetComponent<MetaCollision>().TriggerFall();
        }
    }

    public void TriggerFall() {
        actualState = FallState.Wait;
        timeCollector = 0;
        timeCollector2 = 0;
        FindAllUpNeighbors();
    }

    private const float TIME_TO_WAIT = 2f;
    private const float WIGGLE_AROUND = 0.2f;
    private float timeCollector;
    private float timeCollector2;

    private const float FALL_ACCELERATION = 0.2f;
    private const float SPEED_CAP = 0.8f;
    private float speed;

    void Update() {
        if(actualState == FallState.Wait) {
            timeCollector += Time.deltaTime;
            timeCollector2 += Time.deltaTime;

            if(timeCollector2 >= WIGGLE_AROUND) {
                Wiggle();
                timeCollector2 = 0;
            }
            if(timeCollector >= TIME_TO_WAIT) {
                spriteOffset.transform.localPosition = Vector3.zero;
                TriggerFallOnUpwards();
                actualState = FallState.Fall;
                speed = 0;
            }
        }
    }

    private int lastWiggle;
    private Vector3[] wiggleDirections = {
                                             -Vector3.right * 0.02f,
                                             Vector3.up * 0.02f,
                                             Vector3.right * 0.02f,
                                             -Vector3.up * 0.02f
                                         };

    private void Wiggle() {
        int extracted;
        if(lastWiggle == -1)
            lastWiggle = extracted = Random.Range(0, 3);
        else {
            extracted = lastWiggle > 1 ? lastWiggle - 2 : lastWiggle + 2;
            lastWiggle = -1;
        }

        Vector3 direction = wiggleDirections[extracted];

        spriteOffset.transform.localPosition += direction;
    }

    void FixedUpdate() {
        if(actualState == FallState.Fall) {
            speed = Mathf.Min(speed + (FALL_ACCELERATION * Time.fixedDeltaTime), SPEED_CAP);
            float haveToStopIn = float.PositiveInfinity;
            for(int i = 0; i < downBlocks.Count; i++) {
                Vector2 startingPosition = new Vector2(downBlocks[i].position.x, downBlocks[i].position.y - 0.51f);
                RaycastHit2D hit = Physics2D.Raycast(startingPosition, -Vector2.up, 1f, blockLayerMask);
                if(hit.collider != null) {
                    float tmp = (startingPosition - hit.point).magnitude;
                    if(tmp < haveToStopIn)
                        haveToStopIn = tmp;
                }
            }

            float oldY = myTransform.position.y;
            myTransform.position += (-Vector3.up * speed);
            float newY = myTransform.position.y;

            float difference = oldY - newY;
            if(difference >= haveToStopIn) {
                myTransform.position += Vector3.up * (difference - haveToStopIn);
                actualState = FallState.Still;
            }
        }
    }
}
