﻿using UnityEngine;
using System.Collections;

public class TileType : MonoBehaviour {
    public int points;

    public Sprite[] sprites;
    public SpriteRenderer sRenderer;

    void Awake() {
        points = 0;
    }

    public void SetType() {
        sRenderer.sprite = sprites[(int)points];
    }
}