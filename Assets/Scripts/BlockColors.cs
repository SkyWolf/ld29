﻿public enum BlockColors {
    Air,
    Red,
    Blue,
    Green,
    Yellow,
    Pink,
    NUM_COLORS
}