﻿using UnityEngine;
using System.Collections;

public static class Pieces {
    public static bool[,] tPiece = new bool[,] {
        {false, true,   false},
        {true,  true,   true}
    };
    public static bool[,] tPieceRev = new bool[,] {
        {true, true,   true},
        {false,  true, false}
    };
}
