﻿using UnityEngine;
using System.Collections;

public class DrilloController : MonoBehaviour {
    private Transform myTransform;
    private BoxCollider2D myCollider;
    private Rigidbody2D myRigidbody;
    private Animator myAnimator;
    private AudioSource myAudioSource;
    private int blockLayerMask;
    private int wallLayerMask;
    private int wallLayerNonBit;
    private SpriteRenderer sRenderer;

    void Awake() {
        myTransform = transform;
        myRigidbody = rigidbody2D;
        myAnimator = GetComponent<Animator>();
        blockLayerMask = 1 << LayerMask.NameToLayer("Block");
        wallLayerMask = 1 << LayerMask.NameToLayer("Wall");
        wallLayerNonBit = LayerMask.NameToLayer("Wall");
        sRenderer = GetComponent<SpriteRenderer>();
        myCollider = GetComponent<BoxCollider2D>();
        myAudioSource = audio;
    }

    private void OnEnable() {
        sRenderer.sortingOrder = 0;
        InitializeValues();
    }

    private void OnDisable() {
        sRenderer.sortingOrder = 1;
    }

    private void InitializeValues() {
        hasToJump = false;
        isJumping = false;
        hasToMoveOrAttackSideways = 0;
        hasToAttackDown = false;
        isAttacking = false;

        isAttackReady = true;

        oxygen = startingOxygen;
        timeCollector = 0;
        timeCollector2 = 0;
    }

    public RestartManager restarter;
    public AudioClip die;
    private void Die() {
        myCollider.enabled = false;
        myRigidbody.AddForce((Vector2.up + (Vector2.right * ((Random.value * 0.8f) - 0.4f))) * 150);
        myAudioSource.PlayOneShot(die);
        this.enabled = false;

        restarter.ActivateIn(2);
    }

    private void Win() {
        myCollider.enabled = false;
        this.enabled = false;

        restarter.ActivateIn(2);
    }

    private bool hasToJump = false;
    private bool isJumping = false;
    private int hasToMoveOrAttackSideways = 0;
    private bool hasToAttackDown = false;
    private bool isAttacking = false;

    public SetLevel bar;
    private bool isAttackReady = true;
    public float timeToRechargeAttack = 1.5f;
    private float timeCollector = 0;

    public OxygenManager oBar;
    private float oxygen;
    private float oldOxygen;
    public float startingOxygen = 100;
    public float oxygenReductionPerSecond = 3;
    private float timeCollector2 = 0;

    public AudioClip oxygenAlarm;

    void Update() {
        oldOxygen = oxygen;
        oxygen = Mathf.Max(0, oxygen - (oxygenReductionPerSecond * Time.deltaTime));
        oBar.ShowOxygen(oxygen / startingOxygen);
        if(oxygen < 0.15 * startingOxygen && oldOxygen >= 0.15 * startingOxygen)
            myAudioSource.PlayOneShot(oxygenAlarm);
        if(oxygen == 0)
            Die();

        if(!isAttackReady) {
            timeCollector += Time.deltaTime;
            bar.Setup(Mathf.Min(timeCollector / timeToRechargeAttack, 1));

            if(timeCollector >= timeToRechargeAttack)
                isAttackReady = true;
        }

        if(Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            hasToJump = true;
        else if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            hasToMoveOrAttackSideways = -1;
        else if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            hasToMoveOrAttackSideways = 1;
        else if(Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            hasToAttackDown = true;
    }

    public AudioClip hitGround;
    public AudioClip jump;
    void FixedUpdate() {
        if(isJumping) {
            Vector2 startingPosition = new Vector2(myTransform.position.x, myTransform.position.y);
            RaycastHit2D hit = Physics2D.Raycast(startingPosition, -Vector2.up, 0.05f, blockLayerMask);
            if(hit.collider != null) {
                isJumping = false;
                myAudioSource.PlayOneShot(hitGround, 0.55f);
                myAnimator.SetBool("Jumping", isJumping);
            }
        }
        if(hasToJump) {
            if(!isJumping && !isAttacking) {
                myRigidbody.AddForce(Vector2.up * 250);

                myAudioSource.PlayOneShot(jump);

                hasToJump = false;
                isJumping = true;

                myAnimator.SetBool("Jumping", isJumping);
            }
        }
        else if(hasToMoveOrAttackSideways != 0) {
            if(!isAttacking) {
                Vector2 direction = Vector2.right * Mathf.Sign(hasToMoveOrAttackSideways);

                Vector2 startingPosition = new Vector2(myTransform.position.x, myTransform.position.y + 0.01f);

                int layermask = blockLayerMask | wallLayerMask;

                RaycastHit2D hit = Physics2D.Raycast(startingPosition, direction, 1f, layermask);
                if(hit.collider == null) {
                    myTransform.position += Vector3.right * Mathf.Sign(hasToMoveOrAttackSideways);
                    isJumping = true;
                }
                else if(!isJumping && isAttackReady && hit.collider.gameObject.layer != wallLayerNonBit) {
                    Attack(direction);
                }

                hasToMoveOrAttackSideways = 0;
            }
        }
        else if(hasToAttackDown) {
            if(!isJumping && isAttackReady) {
                Attack(-Vector2.up);
            }
            hasToAttackDown = false;
        }
    }

    private void Attack(Vector2 direction) {
        isAttackReady = false;
        timeCollector = 0;

        Vector2 startingPosition = new Vector2(myTransform.position.x, myTransform.position.y + 0.01f);
        RaycastHit2D hit = Physics2D.Raycast(startingPosition, direction, 0.75f, blockLayerMask);
        if(hit.collider != null) {
            isAttacking = true;
            switch(Mathf.RoundToInt(direction.x)) {
                case -1:
                    myAnimator.SetTrigger("AttackLeft");
                    break;
                case 0:
                    myAnimator.SetTrigger("AttackDown");
                    break;
                case 1:
                    myAnimator.SetTrigger("AttackRight");
                    break;
            }
            StartCoroutine(DestroyAfter(0.6f, hit.collider.gameObject));
        }
    }

    public AudioClip attackGround;
    private IEnumerator DestroyAfter(float time, GameObject toDestroy) {
        yield return new WaitForSeconds(time);
        toDestroy.collider2D.enabled = false;
        toDestroy.GetComponent<MetaCollision>().DetachEverything();
        myAudioSource.PlayOneShot(attackGround, 0.8f);
        Destroy(toDestroy);
        isAttacking = false;
        isJumping = true;
    }

    private const float TOLERANCE = 0.05f;
    void OnCollisionEnter2D(Collision2D col) {
        GameObject theHit = col.gameObject;
        if(theHit.layer == wallLayerNonBit)
            return;

        MetaCollision collision = theHit.GetComponent<MetaCollision>();
        if(collision.actualState == MetaCollision.FallState.Fall) {
            ContactPoint2D[] points = col.contacts;
            if(Mathf.Abs(points[0].point.y - myTransform.position.y) > TOLERANCE) {
                Die();
            }
        }
    }

    public AudioClip pickupOxygen;
    void OnTriggerEnter2D(Collider2D col) {
        if(col.gameObject.tag == "Oxygen") {
            myAudioSource.PlayOneShot(pickupOxygen);
            oxygen = Mathf.Min(startingOxygen, oxygen + startingOxygen * 0.3f);
            col.gameObject.GetComponent<Collapse>().CollapseOn(myTransform);
        }
        else if(col.gameObject.tag == "FinishLine") {
            Win();
        }
    }
}