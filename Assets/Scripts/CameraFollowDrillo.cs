﻿using UnityEngine;
using System.Collections;

public class CameraFollowDrillo : MonoBehaviour {
    public Transform character;
    private Transform myTransform;

    void Awake() {
        myTransform = transform;
    }

    void Update() {
        if(character != null)
            myTransform.position = new Vector3(myTransform.position.x, character.position.y, myTransform.position.z);
    }
}