﻿using UnityEngine;
using System.Collections.Generic;

public class SectionCreator : MonoBehaviour {
    public int sectionWidth;
    public int sectionHeight;
    private int initializedWidth;
    private int initializedHeight;

    private BlockColors[,] sectionColors;
    private System.Random uniRandom;

    public GameObject tilePrefab;
    public GameObject metaPrefab;
    public GameObject wallPrefab;
    private Transform myTransform;

    public Transform blocksContainer;
    public Transform wallsContainer;

    public GameObject oxygenPrefab;

    void Awake() {
        uniRandom = new System.Random();
        myTransform = transform;

        CreateSection();
        CreateSprites();
        CreateWalls();
    }

    void CreateSection() {
        //Clean up, just in case
        initializedWidth = sectionWidth;
        initializedHeight = sectionHeight;

        sectionColors = new BlockColors[initializedWidth, initializedHeight];
        for(int i = 0; i < initializedWidth; i++) {
            for(int j = 0; j < initializedHeight; j++) {
                sectionColors[i, j] = BlockColors.Air;
            }
        }

        List<BlockColors> list = new List<BlockColors>();

        //Randomize
        for(int i = 0; i < initializedWidth; i++) {
            for(int j = 0; j < initializedHeight; j++) {
                if (!(uniRandom.NextDouble() < 0.1f)) {
                    list.Clear();
                    for(int c = 1; c < (int)BlockColors.NUM_COLORS; c++) {
                        list.Add((BlockColors)c);
                    }

                    if(i > 0 && sectionColors[i - 1, j] != BlockColors.Air)
                        list.Add(sectionColors[i - 1, j]);
                    if(j > 0 && sectionColors[i, j - 1] != BlockColors.Air)
                        list.Add(sectionColors[i, j - 1]);

                    sectionColors[i, j] = list[uniRandom.Next(list.Count)];
                }
            }
        }
    }

    private void CreateWalls() {
        float leftX = ((-initializedWidth / 2) - 1) + (initializedWidth % 2 == 0 ? 0.5f : 0);
        float rightX = ((initializedWidth / 2) + 1) - (initializedWidth % 2 == 0 ? 0.5f : 0);

        for(int i = -2; i < initializedHeight + 1; i++) {
            GameObject left = (GameObject)Instantiate(wallPrefab);
            Transform leftT = left.transform;
            GameObject right = (GameObject)Instantiate(wallPrefab);
            Transform rightT = right.transform;

            left.transform.parent = wallsContainer;
            right.transform.parent = wallsContainer;

            leftT.localPosition = new Vector2(leftX, -i + 0.5f);
            rightT.localPosition = new Vector2(rightX, -i + 0.5f);
        }
    }

    private void CreateSprites() {
        Transform[,] transforms = new Transform[initializedWidth, initializedHeight];
        float startingX = (-initializedWidth / 2) + (initializedWidth % 2 == 0 ? 0.5f : 0);
        float oxygenProb = 0.1f;
        for(int i = 0; i < initializedWidth; i++) {
            for(int j = 0; j < initializedHeight; j++) {
                if(sectionColors[i, j] != BlockColors.Air) {
                    GameObject newTile = (GameObject)Instantiate(tilePrefab);
                    Transform newTileT = newTile.transform;
                    TileColor newTileC = newTile.GetComponent<TileColor>();
                    TileType newTileType = newTile.GetComponent<TileType>();

                    newTileT.localPosition = new Vector3(startingX + (i), -j - 0.5f, 0);
                    newTileC.SetColor(sectionColors[i, j]);

                    if(i > 0) {
                        if(transforms[i - 1, j] != null && transforms[i - 1, j].GetComponent<TileColor>().actualColor == newTileC.actualColor) {
                            newTileT.parent = transforms[i - 1, j].parent;

                            newTileType.points += 1;
                            newTileType.SetType();

                            TileType leftTileType = transforms[i - 1, j].GetComponent<TileType>();
                            leftTileType.points += 4;
                            leftTileType.SetType();
                        }
                    }
                    if(j > 0) {
                        if(transforms[i, j - 1] != null && transforms[i, j - 1].GetComponent<TileColor>().actualColor == newTileC.actualColor) {
                            if(newTileT.parent != null)
                                TransferMetaObject(transforms[i, j - 1].parent, newTileT.parent);
                            else
                                newTileT.parent = transforms[i, j - 1].parent;

                            newTileType.points += 2;
                            newTileType.SetType();

                            TileType leftTileType = transforms[i, j - 1].GetComponent<TileType>();
                            leftTileType.points += 8;
                            leftTileType.SetType();
                        }
                    }

                    if(newTileT.parent == null) {
                        GameObject newMetaObject = (GameObject)Instantiate(metaPrefab);
                        Transform newMetaObjectT = newMetaObject.transform;

                        newMetaObject.name = "Whatever";
                        newMetaObjectT.parent = blocksContainer;
                        newTileT.parent = newMetaObject.GetComponent<MetaCollision>().spriteOffset;
                    }

                    transforms[i, j] = newTileT;
                }
                else {
                    if(uniRandom.NextDouble() > oxygenProb) {
                        oxygenProb += 0.2f;
                        GameObject oxy = (GameObject)Instantiate(oxygenPrefab);
                        oxy.transform.localPosition = new Vector3(startingX + (i), -j - 0.5f, 0);
                    }
                }
            }
        }

        int metas = blocksContainer.childCount;
        for(int i = 0; i < metas; i++) {
            blocksContainer.GetChild(i).GetComponent<MetaCollision>().CreateMetaCollider();
        }
    }

    private void TransferMetaObject(Transform fromThis, Transform toThat) {
        int childCount = fromThis.childCount;
        for(int i = 0; i < childCount; i++)
            fromThis.GetChild(0).parent = toThat;

        if(fromThis.childCount == 0)
            GameObject.Destroy(fromThis.gameObject);
    }
}
