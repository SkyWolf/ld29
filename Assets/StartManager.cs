﻿using UnityEngine;
using System.Collections;

public class StartManager : MonoBehaviour {
    public ClockManager clock;
    public DrilloController drillo;

	void Update () {
        if(Input.GetKeyDown(KeyCode.S)) {
            clock.enabled = true;
            drillo.enabled = true;
            gameObject.SetActive(false);
        }
	}
}
