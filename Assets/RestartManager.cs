﻿using UnityEngine;
using System.Collections;

public class RestartManager : MonoBehaviour {
    public ClockManager clock;
    public DrilloController drillo;
    public GameObject restarter;

    private float activateIn;
    private float timeCollector;

    void Update() {
        if(restarter.activeSelf) {
            if(Input.GetKeyDown(KeyCode.R)) {
                Application.LoadLevel(0);
            }
        }
        else {
            timeCollector += Time.deltaTime;
            if(timeCollector >= activateIn)
                restarter.SetActive(true);
        }
    }

    public void ActivateIn(float seconds) {
        activateIn = seconds;

        clock.enabled = false;
        drillo.enabled = false;
        this.enabled = true;
    }
}
