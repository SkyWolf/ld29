﻿using UnityEngine;
using System.Collections;

public class NumberRepresent : MonoBehaviour {
    public Sprite[] numbers;
    public SpriteRenderer sRenderer;
    
    public void Represent(int toRepresent) {
        sRenderer.sprite = numbers[toRepresent];
    }
}
