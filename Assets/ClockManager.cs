﻿using UnityEngine;
using System.Collections;

public class ClockManager : MonoBehaviour {
    public NumberRepresent centUN, centDE;
    public NumberRepresent secoUN, secoDE;
    public NumberRepresent minuUN, minuDE;

    private float time;

    void Update() {
        time += Time.deltaTime;

        int seconds = (int)(time % 60);
        int minutes = (int)(time / 60);
        float cent = time - (int)time;
        int hundreds = (int)(cent * 100);

        Debug.Log(seconds);

        centUN.Represent(hundreds % 10);
        centDE.Represent(hundreds / 10);

        secoUN.Represent(seconds % 10);
        secoDE.Represent(seconds / 10);

        minuUN.Represent(minutes % 10);
        minuDE.Represent(minutes / 10);
    }

    private void Reset() {
        time = 0;
    }

    private void Start() {
        this.enabled = true;
    }

    private void Stop() {
        this.enabled = false;
    }
}
