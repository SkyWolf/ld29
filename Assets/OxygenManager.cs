﻿using UnityEngine;
using System.Collections;

public class OxygenManager : MonoBehaviour {
    public Transform oxygenCounter;
    public Transform emptyOxygen;
    public Transform fullOxygen;

    public void ShowOxygen(float percentage) {
        oxygenCounter.position = Vector3.Lerp(emptyOxygen.position, fullOxygen.position, percentage);
    }
}
