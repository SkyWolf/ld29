﻿using UnityEngine;
using System.Collections;

public class Collapse : MonoBehaviour {
    private Transform collapseUpon;
    private Transform myTransform;
    private Collider2D myCollider;

    public float collapseIn;
    private float timeCollector;

    void Awake() {
        myCollider = GetComponent<BoxCollider2D>();
        myTransform = transform;
    }

    void Update() {
        timeCollector += Time.deltaTime;

        myTransform.position = Vector3.Lerp(myTransform.position, collapseUpon.position, timeCollector / collapseIn);
        myTransform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, timeCollector / collapseIn);

        if(timeCollector >= collapseIn)
            Destroy(gameObject);
    }

    public void CollapseOn(Transform me) {
        collapseUpon = me;
        myCollider.enabled = false;
        this.enabled = true;
    }
}
